import React from "react"
import { Container } from "react-bootstrap"

export function Header(props) {
  return (
    <Container>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">Give it Away</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-item nav-link" href="/contact">Join the Stand By List</a>
        <a class="nav-item nav-link" href="https://www.nhscharitiestogether.co.uk/">Donate to NHS Charities Together</a>

      </div>
    </div>
  </nav>
    </Container>
  )
}
