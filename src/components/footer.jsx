import React from "react"
import { Container } from "react-bootstrap"
import { Link } from "./link"

export function Footer(props) {
  return (
    <Container class="text-center">
      <br/><br/><br/>
      <br/><br/><br/>
      <p style={{textAlign:"center"}}> Give It Away UK is not a business, a charity or anything in between. This website operates for no profit or commercial gain. Your data and privacy rights are protected under GDPR - https://gdpr-info.eu </p>
    </Container>
  )
}
