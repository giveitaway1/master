import React from "react"
import { Container, Button, Jumbotron, Row, } from "react-bootstrap"
import Layout from "../components/layout"

export default () => (
  <Layout>
    <Container>
      <Jumbotron>
        <h1>An unofficial stand by list for the Covid-19 vaccine</h1>
        <p><b>
         We set up this service because we've heard there are many 'no shows' in the UK at vaccination centres. To ensure no vaccines go to waste, we've set up a free service to allow healthcare workers to confidentially contact people last minute to come to a local centre and receive the vaccine. Join the waiting list below.
        </b></p>
        <p>
          1. Give It Away is not an official website and is no way affilaited with the NHS, the UK government or any other official body.
        </p>
        <p>
          2. Joining the waiting list is no guarantee of a vaccine and should not be seen as an alternative to registering with your GP or local health authority in order to get the vaccine via the usual method.
        </p>
        <p>
          3. Your data: last name, age, first 3 digits of your postcode, mobile number and email address, will be processed by Give It Away and shared with healthcare officials in order to contact you. We will do reasonable checks to ensure your data is only shared with healthcare offiicals at vaccination centres or similar. We will not be held responsible for any missuse of your data thereafter.
        </p>
        <p>
          4. Give It Away is not a business or a charity. This service is free. All data will be deleted from Give It Away by October 2021 or earlier if vaccination efforts progress as planned.
        </p>
        <p>
          5. If a call or email is received you will drop everything (within reason) to attend your local vaccination centre at their request.
        </p>

        <p>
        In joining the waiting list via the button below - you acknowledge and accept the terms outlined in 1 - 5
        </p>
        <p>
          <Button variant="primary "href="/contact">Join the stand by list</Button>
        </p>

      </Jumbotron>
      <Row>
      <div className="col-md-4">
        <h2>For Vaccination Staff</h2>
        <p>Contact the waiting list via the email link below. Please enclose an attachment proving you work for a vaccination centre. Once verified - we will send you an updated daily list within 5 miles of your centre. Our email server is in Switzerland and we undertake never to reveal any of your personal or professional information. </p>
        <p><a className="btn btn-default" href="mailto:rubbishmanagement@protonmail.com" role="button">Email Now »</a></p>
      </div>
      <div className="col-md-4">
        <h2>Vaccination Centres in the UK</h2>
        <p>The link below will take you to a list of local vaccination centres in England, Wales and Scotland </p>
        <p><a className="btn btn-default" href="https://www.england.nhs.uk/coronavirus/publication/vaccination-sites/" role="button">View details »</a></p>
     </div>
      <div className="col-md-4">
        <h2>Processing of Data</h2>
        <p>Your data will be protected in a secure environment and only shared, with your consent, with people we reasonably believe to be healthcare workers at vaccination centres. We have done our best to mitigate against sharing sensitive data that could lead to missuse. Your rights are protected under GDPR - view the full legal text by clicking below. </p>
        <p><a className="btn btn-default" href="https://gdpr-info.eu" role="button">View details »</a></p>
      </div>
      </Row>


    </Container>
  </Layout>



)
