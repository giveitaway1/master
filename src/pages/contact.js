import React, { useState }  from "react"
import axios from "axios";
import { Link } from "gatsby"
import Layout from "../components/layout"


const MyForm = () => {

    const [serverState, setServerState] = useState({
      submitting: false,
      status: null
    });
    const handleServerResponse = (ok, msg, form) => {
      setServerState({
        submitting: false,
        status: { ok, msg }
      });
      if (ok) {
        form.reset();
      }
    };
    const handleOnSubmit = e => {
      e.preventDefault();
      const form = e.target;
      setServerState({ submitting: true });
      axios({
        method: "post",
        url: "https://app.getform.io/forms/25605",
        data: new FormData(form)
      })
        .then(r => {
          handleServerResponse(true, "Thanks!", form);
        })
        .catch(r => {
          handleServerResponse(false, r.response.data.error, form);
        });
    };
    return (
        <Layout>

    <div>
         <div className="col-md-8 mt-5">
            <h3>Unofficial Stand By List</h3>
            <form onSubmit={handleOnSubmit}>
            <div className="form-group">
                <label for="exampleInputEmail1" required="required">Email address</label>
                <input type="email" name="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"/>
            </div>
            <div className="form-group">
                <label for="exampleInputName">Last Name</label>
                <input type="text" name="name" className="form-control" id="exampleInputName" placeholder="Enter your name" required="required"/>
            </div>
            <div className="form-group">
                <label for="exampleInputAge">Age</label>
                <input type="text" name="age" className="form-control" id="exampleInputName" placeholder="Enter your age" required="required"/>
            </div>
            <div className="form-group">
                <label for="exampleInputPostcode">First 3 letters of your postcode</label>
                <input type="text" name="postcode" className="form-control" id="exampleInputName" placeholder="Enter your postcode" required="required"/>
            </div>
            <div className="form-group">
                <label for="exampleFormControlSelect1">Do you conset to your data being used for the purposes of contacting you?</label>
                <select className="form-control" id="exampleFormControlSelect1" name="platform" required="required">
                <option>No</option>
                <option>Yes</option>
                </select>
            </div>
            <button type="submit" className="btn btn-primary"  disabled={serverState.submitting}>
                Submit
            </button>
            {serverState.status && (
                <p className={!serverState.status.ok ? "errorMsg" : ""}>
                {serverState.status.msg}
                </p>
            )}
            </form>
        </div>
      </div>

  </Layout>

    );
  };

  export default MyForm;
